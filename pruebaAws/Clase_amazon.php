<?php

	if (!defined('BASEPATH')) {
		exit('No direct script access allowed');
	}

	//namespace aws\Aws\S3;
	
	require 'aws/aws-autoloader.php';

	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception ;

	/**
	 *Clase para gestion de  archivos en amazonS3 
	 */

	class Clase_amazon {

		private $CI;

		public function __construct()
		{
			$this->CI = &get_instance();
		}

		/**
		 * Metodo para guardar los archivos en bucket de amazon s3
		 * @param string $filePath
		 * @param string $nameFolder
		 */
		public function uploadFile($filePath, $nameFolder, $temp, $ext){
			

			$bucketName  =  'devuploadfile' ;
			//$filePath  =  './articles-186370_constitucion_politica.pdf' ;
			$keyName  =  basename ( $nameFolder.".pdf");
			$IAM_KEY  =  'AKIAVHOZ4VIDA4PZ4R7L' ;
			$IAM_SECRET  = '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' ;
			$msg = 'El archivo se ha subido con éxito';
			$success = true;
		
		

			// Set Amazon S3 Credentials
			$s3 = S3Client::factory(
				array(
					'credentials' => array(
						'key' => $IAM_KEY,
						'secret' => $IAM_SECRET
					),
					'version' => 'latest',
					'region'  => 'us-east-2'
				)
			);
			
			// The region matters. I'm using "US Ohio" so "us-east-2" is the corresponding
			// region code. You can google it or upload a file to the S3 bucket and look at
			// the public url. It will look like:
			// https://s3.us-east-2.amazonaws.com/YOUR_BUCKET_NAME/image.png
			// 
			// As you can see the us-east-2 in the url.

			try {
				// So you need to move the file on $filePath to a temporary place.
				// The solution being used: http://stackoverflow.com/questions/21004691/downloading-a-file-and-saving-it-locally-with-php
				
				if (!file_exists('/tmp/tmpfile')) {
					mkdir('/tmp/tmpfile');
				}
				
				// Create temp file
				// $tempFilePath = $temp;//''.$temp.'' . basename($filePath);
				// $tempFile = fopen($tempFilePath, "w") or die("Error: Unable to open file.");
				// $fileContents = file_get_contents($temp);
				// $tempFile = file_put_contents($tempFilePath, $fileContents);
				
				
				// Put on S3
				$s3->putObject(
					array(
						'Bucket'=>$bucketName,
						'Key' =>  $keyName,
						'SourceFile' => $temp,
						'ACL'        => 'public-read-write',
						'StorageClass' => 'REDUCED_REDUNDANCY'
					)
				);

				// se consulta la url del archivo

				// $objects = $s3->listObjectsV2([
				// 	'Bucket' => $bucketName,
				// 	'Prefix' => $nameFolder
				// ]);

				// $cmd = $s3->getCommand('GetObject', [
				// 	'Bucket' => $bucketName,
				// 	'Key'    => $objects['Contents'][0]["Key"]
				// ]);

				//Getting the URL to an object
				$url = $s3->getObjectUrl($bucketName, $keyName);

				echo $url."<br>" ;

				//$signed_url = $s3->createPresignedRequest($cmd, '+1 hour');

				return array('success' => $success, 'msg' => $msg, 'path' => $url, 'extension' => $ext, 'name' => $keyName);

			} catch (S3Exception $e) {
				return $e->getMessage();
			} catch (Exception $e) {
				return $e->getMessage();
			}
		}


		/**
		 * Metodo para consultar los archivos amazon s3
		 * @param string $filePath
		 * @param string $nameFolder
		 */
		public function getFile($nameFolder){
			
			// credenciales para cliente amazonS3
			$bucketName = 'devuploadfile';
			$keyname = $nameFolder;
			$IAM_KEY  =  'AKIAVHOZ4VIDA4PZ4R7L' ;
			$IAM_SECRET  = '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' ;

			// Set Amazon S3 Credentials
			$s3 = S3Client::factory(
				array(
					'credentials' => array(
						'key' => $IAM_KEY,
						'secret' => $IAM_SECRET
					),
					'version' => 'latest',
					'region'  => 'us-east-2'
				)
			);

			try {

				#listObjectsV2 consulta los archivos que estan dentro de la carpeta que se quiere consultar
				# para consultar dentro de una carpeta se necesita  untilizar prefix 
				# si solo se van a consultar los archivos que estan en el Bucket solo se deja esa opción 
				$objects = $s3->listObjectsV2([
					'Bucket' => $bucketName,
					'Prefix' => $nameFolder
				]);


				// se recorre el contenido que trae el metodo listObjectsV2
				foreach ($objects['Contents'] as $object){
					// consulto solo el nombre del archivo ya que el dato viene anidado con el nombre de la carpeta
					$nameFile = explode("/", $object['Key'])[1];
					
					
					$cmd = $s3->getCommand('GetObject', [
						'Bucket' => $bucketName,
						'Key'    => $object['Key']
					]);

					$signed_url = $s3->createPresignedRequest($cmd, '+1 hour');

					echo  "<a href='".$signed_url->getUri()."' target='_blanck' download> ".$nameFile." </a>". "</br>";
				}

			} catch (S3Exception $e) {
				echo $e->getMessage() . PHP_EOL;
			}
		}
	}
?>